#include <cg/config.hpp>
#include <cg/component.hpp>
#include <tester/metacg.hpp>

BOOST_FIXTURE_TEST_SUITE(PartialHana, tester::Metacg)

struct Config {
    BOOST_HANA_DEFINE_STRUCT(Config,
        (int, a),
        (std::string, b)
    );
};

template<const char *P, int A>
struct Component : cg::Component
{
    template<typename>
    struct Types
    {
        using Config = PartialHana::Config;
    };

    template<typename Resolver>
    struct State
    {
        typename Types<Resolver>::Config config;
        CONFIG_DECLARE_STATE_CONSTRUCTORS(config) {}
    };

    struct Service
    {
        template<typename Base>
        struct Interface : Base
        {
            int run()
            {
                BOOST_TEST(this->state.config.a == A);
                BOOST_TEST(this->state.config.b == P);
                return 0;
            }
        };
    };

    using Services = ct::tuple<Service>;
};

static constexpr char Aarg[] = "foo";
static constexpr char Barg[] = "test";

struct A: Component<Aarg, 1> {};
struct B: Component<Barg, 2> {};

using G = cg::Graph<A, B>;

BOOST_AUTO_TEST_CASE(FromFile)
{
    test_graph<G>(std::filesystem::path(__FILE__).replace_extension(".json"), cg::Args<B>(Config{2, "test"}));
}

BOOST_AUTO_TEST_CASE(FromArgs)
{
    test_graph<G>(cg::Args<A>(Config{1, "foo"}), cg::Args<B>(Config{2, "test"}));
}

BOOST_AUTO_TEST_SUITE_END()
