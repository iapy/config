#include <cg/config.hpp>
#include <cg/component.hpp>
#include <tester/metacg.hpp>

namespace test::foo::bar
{
    struct A : cg::Component
    {
        template<typename>
        struct State
        {
            std::string a;
            int b;
            int c;

            CONFIG_DECLARE_STATE_CONSTRUCTORS(a, b)
            , c{static_cast<int>(a.size()) + b}
            {}
        };

        struct Service
        {
            template<typename Base>
            struct Interface : Base
            {
                int run()
                {
                    BOOST_TEST(this->state.a == "foo");
                    BOOST_TEST(this->state.b == 42);
                    BOOST_TEST(this->state.c == 45);
                    return 0;
                }
            };
        };

        using Services = ct::tuple<Service>;
    };
} // namespace test::foo::bar

BOOST_FIXTURE_TEST_SUITE(Namespace, tester::Metacg)

using G = cg::Graph<test::foo::bar::A>;

BOOST_AUTO_TEST_CASE(FromFile)
{
    test_graph<G>(std::filesystem::path(__FILE__).replace_extension(".json"));
}

BOOST_AUTO_TEST_CASE(FromArgs)
{
    using namespace test::foo::bar;
    test_graph<G>(cg::Args<A>("foo", 42));
}

BOOST_AUTO_TEST_SUITE_END()

