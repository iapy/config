#include <cg/config.hpp>
#include <cg/component.hpp>
#include <tester/metacg.hpp>

struct TestAbsentTypes : tester::Metacg
{
    struct tag
    {
        struct B{};
    };
};

BOOST_FIXTURE_TEST_SUITE(Absent, TestAbsentTypes)

using tag = TestAbsentTypes::tag;

struct A : cg::Component
{
    template<typename>
    struct State
    {
        int a;
        std::string b;

        State() : State{42, "foo"} {}
        CONFIG_DECLARE_STATE_CONSTRUCTORS(a, b) {}
    };

    struct Service
    {
        template<typename Base>
        struct Interface : Base
        {
            int run()
            {
                BOOST_TEST(this->state.a == 42);
                BOOST_TEST(this->state.b == "foo");
                BOOST_TEST(this->remote(tag::B{}).get() == "bar");
                return 0;
            }
        };
    };

    using Services = ct::tuple<Service>;
};

struct B : cg::Component
{
    template<typename>
    struct State
    {
        std::string a;
        CONFIG_DECLARE_STATE_CONSTRUCTORS(a) {}
    };

    struct Impl
    {
        template<typename Base>
        struct Interface : Base
        {
            std::string get()
            {
                return this->state.a;
            }
        };
    };

    using Ports = ct::map<ct::pair<tag::B, Impl>>;
};

using G = cg::Graph<cg::Connect<A, B>>;

BOOST_AUTO_TEST_CASE(FromFile)
{
    test_graph<G>(std::filesystem::path(__FILE__).replace_extension(".json"));
}

BOOST_AUTO_TEST_CASE(FromArgs)
{
    test_graph<G>(cg::Args<B>(std::string("bar")));
}

BOOST_AUTO_TEST_SUITE_END()
