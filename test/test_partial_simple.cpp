#include <cg/config.hpp>
#include <cg/component.hpp>
#include <tester/metacg.hpp>

BOOST_FIXTURE_TEST_SUITE(PartialSimple, tester::Metacg)

template<const char *P, int A, int B>
struct Component : cg::Component
{
    template<typename>
    struct State
    {
        std::string a;
        int b;
        int c;

        CONFIG_DECLARE_STATE_CONSTRUCTORS(a, b)
        , c{static_cast<int>(a.size()) + b}
        {}
    };

    struct Service
    {
        template<typename Base>
        struct Interface : Base
        {
            int run()
            {
                BOOST_TEST(this->state.a == P);
                BOOST_TEST(this->state.b == A);
                BOOST_TEST(this->state.c == B);
                return 0;
            }
        };
    };

    using Services = ct::tuple<Service>;
};

static constexpr char Aarg[] = "foo";
static constexpr char Barg[] = "test";

struct A: Component<Aarg, 1, 4> {};
struct B: Component<Barg, 2, 6> {};

using G = cg::Graph<A, B>;

BOOST_AUTO_TEST_CASE(FromFile)
{
    test_graph<G>(std::filesystem::path(__FILE__).replace_extension(".json"), cg::Args<B>("test", 2));
}

BOOST_AUTO_TEST_CASE(FromArgs)
{
    test_graph<G>(cg::Args<A>("foo", 1), cg::Args<B>("test", 2));
}

BOOST_AUTO_TEST_SUITE_END()
