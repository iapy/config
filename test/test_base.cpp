#include <cg/config.hpp>
#include <cg/component.hpp>
#include <tester/metacg.hpp>

BOOST_FIXTURE_TEST_SUITE(Base, tester::Metacg)

struct Tag {};

struct AState
{
    int const a;
    std::string const b;

    CONFIG_DECLARE_STATE_BASE_CONSTRUCTORS(AState, a, b)
    {
    }
};

struct BState
{
    std::string const a;
    CONFIG_DECLARE_STATE_BASE_CONSTRUCTORS(BState, a)
    {
    }
};

struct A : cg::Component
{
    template<typename>
    struct State : AState
    {
        CONFIG_DECLARE_STATE_BASE(AState) {}
    };

    struct Service
    {
        template<typename Base>
        struct Interface : Base
        {
            int run()
            {
                BOOST_TEST(this->state.a == 42);
                BOOST_TEST(this->state.b == "foo");
                BOOST_TEST(this->remote(Tag{}).get() == "bar");
                return 0;
            }
        };
    };

    using Services = ct::tuple<Service>;
};

struct B : cg::Component
{
    template<typename>
    struct State : BState
    {
        CONFIG_DECLARE_STATE_BASE(BState) {}
    };

    struct Impl
    {
        template<typename Base>
        struct Interface : Base
        {
            std::string get()
            {
                return this->state.a;
            }
        };
    };

    using Ports = ct::map<ct::pair<Tag, Impl>>;
};

using G = cg::Graph<cg::Connect<A,B>>;

BOOST_AUTO_TEST_CASE(FromFile)
{
    test_graph<G>(std::filesystem::path(__FILE__).replace_extension(".json"));
}

BOOST_AUTO_TEST_SUITE_END()
