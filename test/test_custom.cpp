#include <cg/config.hpp>
#include <cg/component.hpp>
#include <tester/metacg.hpp>

struct TestCustomTypes : tester::Metacg
{
    struct X
    {
        int a;
        std::string b;
    };
};

namespace jsonio {

template<>
TestCustomTypes::X from<TestCustomTypes::X>(nlohmann::json const &obj)
{
    std::string value = obj.get<std::string>();
    return TestCustomTypes::X{
        std::atoi(value.substr(0, value.find(':')).c_str()),
        value.substr(value.find(':') + 1)
    };
}

} // namespace jsonio

BOOST_FIXTURE_TEST_SUITE(Custom, TestCustomTypes)

struct A : cg::Component
{
    template<typename>
    struct State
    {
        TestCustomTypes::X x;
        CONFIG_DECLARE_STATE_CONSTRUCTORS(x) {}
    };

    struct Service
    {
        template<typename Base>
        struct Interface : Base
        {
            int run()
            {
                BOOST_TEST(this->state.x.a == 42);
                BOOST_TEST(this->state.x.b == "foo");
                return 0;
            }
        };
    };

    using Services = ct::tuple<Service>;
};

using G = cg::Graph<A>;

BOOST_AUTO_TEST_CASE(FromFile)
{
    test_graph<G>(std::filesystem::path(__FILE__).replace_extension(".json"));
}

BOOST_AUTO_TEST_CASE(FromArgs)
{
    test_graph<G>(cg::Args<A>(X{42, "foo"}));
}

BOOST_AUTO_TEST_SUITE_END()
