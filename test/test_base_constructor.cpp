#include <cg/config.hpp>
#include <cg/component.hpp>
#include <tester/metacg.hpp>

BOOST_FIXTURE_TEST_SUITE(BaseConstructor, tester::Metacg)

struct Tag {};

struct AState
{
    int const a;
    std::string const b;
    std::size_t c{0};

    CONFIG_DECLARE_STATE_BASE_CONSTRUCTORS(AState, a, b)
    {
    }
};

struct BState
{
    std::string const a;
    std::size_t const b;
    std::size_t c;
    CONFIG_DECLARE_STATE_BASE_CONSTRUCTORS(BState, a)
    , b{a.length()}
    {
        c = a.length();
    }
};

struct A : cg::Component
{
    template<typename>
    struct State : AState
    {
        CONFIG_DECLARE_STATE_BASE(AState)
        {
            AState::c = AState::b.length();
        }
    };

    struct Service
    {
        template<typename Base>
        struct Interface : Base
        {
            int run()
            {
                BOOST_TEST(this->state.a == 42);
                BOOST_TEST(this->state.b == "foo");
                BOOST_TEST(this->state.c == 3);
                BOOST_TEST(this->remote(Tag{}).template get<std::string>() == "bar");
                BOOST_TEST(this->remote(Tag{}).template get<std::size_t>() == 6);
                return 0;
            }
        };
    };

    using Services = ct::tuple<Service>;
};

struct B : cg::Component
{
    template<typename>
    struct State : BState
    {
        CONFIG_DECLARE_STATE_BASE(BState) {}
    };

    struct Impl
    {
        template<typename Base>
        struct Interface : Base
        {
            template<typename T>
            T get()
            {
                if constexpr (std::is_same_v<T, std::string>)
                {
                    return this->state.a;
                }
                else
                {
                    return this->state.b + this->state.c;
                }
            }
        };
    };

    using Ports = ct::map<ct::pair<Tag, Impl>>;
};

using G = cg::Graph<cg::Connect<A,B>>;

BOOST_AUTO_TEST_CASE(FromFile)
{
    test_graph<G>(std::filesystem::path(__FILE__).replace_extension(".json"));
}

BOOST_AUTO_TEST_SUITE_END()
