#include <cg/config.hpp>
#include <cg/component.hpp>
#include <tester/metacg.hpp>

struct TestPartialCustomTypes : tester::Metacg
{
    struct X
    {
        int a;
        std::string b;
    };
};

namespace jsonio {

template<>
TestPartialCustomTypes::X from<TestPartialCustomTypes::X>(nlohmann::json const &obj)
{
    std::string value = obj.get<std::string>();
    return TestPartialCustomTypes::X{
        std::atoi(value.substr(0, value.find(':')).c_str()),
        value.substr(value.find(':') + 1)
    };
}

} // namespace jsonio

BOOST_FIXTURE_TEST_SUITE(PartialCustom, TestPartialCustomTypes)

template<const char *P, int A>
struct Component : cg::Component
{
    template<typename>
    struct State
    {
        TestPartialCustomTypes::X x;
        CONFIG_DECLARE_STATE_CONSTRUCTORS(x) {}
    };

    struct Service
    {
        template<typename Base>
        struct Interface : Base
        {
            int run()
            {
                BOOST_TEST(this->state.x.a == A);
                BOOST_TEST(this->state.x.b == P);
                return 0;
            }
        };
    };

    using Services = ct::tuple<Service>;
};

static constexpr char Aarg[] = "foo";
static constexpr char Barg[] = "test";

struct A: Component<Aarg, 1> {};
struct B: Component<Barg, 2> {};

using G = cg::Graph<A, B>;

BOOST_AUTO_TEST_CASE(FromFile)
{
    test_graph<G>(std::filesystem::path(__FILE__).replace_extension(".json"), cg::Args<B>(X{2, "test"}));
}

BOOST_AUTO_TEST_CASE(FromArgs)
{
    test_graph<G>(cg::Args<A>(X{1, "foo"}), cg::Args<B>(X{2, "test"}));
}

BOOST_AUTO_TEST_SUITE_END()
