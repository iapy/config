#include <config/options.hpp>
#include <tester/tester.hpp>
#include <sstream>

namespace
{
    struct Suite
    {
        Suite()
        : sbuf{std::cerr.rdbuf(stream.rdbuf())}
        {
        }

        ~Suite()
        {
            std::cerr.rdbuf(sbuf);
        }

        std::ostringstream stream;
        std::streambuf * const sbuf;
    };
}

BOOST_FIXTURE_TEST_SUITE(Config, Suite)

BOOST_AUTO_TEST_CASE(OptionsAll)
{
    std::vector<char const *> opts{"./test", "--foo", "1", "--bar", "2"};

    int foo{10}, bar{20};

    BOOST_TEST(config::parse(std::begin(opts), std::end(opts)
        , config::opt(foo, "foo").help("foo value")
        , config::opt(bar, "bar").help("bar value")
    ));
    BOOST_TEST(1 == foo);
    BOOST_TEST(2 == bar);
}

BOOST_AUTO_TEST_CASE(OptionsDefault)
{
    std::vector<char const *> opts{"./test", "--foo", "1"};

    int foo{10}, bar{20};

    BOOST_TEST(config::parse(std::begin(opts), std::end(opts)
        , config::opt(foo, "foo").help("foo value")
        , config::opt(bar, "bar").help("bar value")
    ));
    BOOST_TEST(1 == foo);
    BOOST_TEST(20 == bar);
}

BOOST_AUTO_TEST_CASE(OptionsVector)
{
    std::vector<char const *> opts{"./test", "--foo", "1", "-f", "2"};

    std::vector<int> foo;
    BOOST_TEST(config::parse(std::begin(opts), std::end(opts)
        , config::opt(foo, "foo", 'f').help("foo value")
    ));
    BOOST_TEST(stream.str().empty());

    std::vector<int> const expected{1, 2};
    BOOST_TEST(foo == expected);
}

BOOST_AUTO_TEST_CASE(OptionsBooleanTrue)
{
    std::vector<char const *> opts{"./test", "--foo"};

    bool foo{false};
    BOOST_TEST(config::parse(std::begin(opts), std::end(opts)
        , config::opt(foo, "foo").help("foo value")
    ));
    BOOST_TEST(stream.str().empty());
    BOOST_TEST(foo);
}

BOOST_AUTO_TEST_CASE(OptionsBooleanFalse)
{
    std::vector<char const *> opts{"./test"};

    bool foo{false};
    BOOST_TEST(config::parse(std::begin(opts), std::end(opts)
        , config::opt(foo, "foo").help("foo value")
    ));
    BOOST_TEST(stream.str().empty());
    BOOST_TEST(!foo);
}

BOOST_AUTO_TEST_CASE(OptionsNoValue)
{
    std::vector<char const *> opts{"./test", "--foo"};

    int foo{10}, bar{20};

    BOOST_TEST(!config::parse(std::begin(opts), std::end(opts)
        , config::opt(foo, "foo").help("foo value")
        , config::opt(bar, "bar").help("bar value")
    ));
    BOOST_TEST(!stream.str().empty());
}

BOOST_AUTO_TEST_CASE(OptionsHelp)
{
    std::vector<char const *> opts{"./test", "-help"};

    int foo{10}, bar{20};

    BOOST_TEST(!config::parse(std::begin(opts), std::end(opts)
        , config::opt(foo, "foo").help("foo value")
        , config::opt(bar, "bar").help("bar value")
    ));
    BOOST_TEST(!stream.str().empty());
}

BOOST_AUTO_TEST_CASE(Arguments)
{
    std::vector<char const *> opts{"./test", "--foo", "1", "2"};
    int foo{10}, bar{20};

    BOOST_TEST(config::parse(std::begin(opts), std::end(opts)
        , config::opt(foo, "foo").help("foo value")
        , config::arg(bar, "bar")
    ));
    BOOST_TEST(stream.str().empty());
    BOOST_TEST(1 == foo);
    BOOST_TEST(2 == bar);
}

BOOST_AUTO_TEST_CASE(ArgumentsMissing)
{
    std::vector<char const *> opts{"./test", "--foo", "1"};
    int foo{10}, bar{20};

    BOOST_TEST(!config::parse(std::begin(opts), std::end(opts)
        , config::opt(foo, "foo").help("foo value")
        , config::arg(bar, "bar")
    ));
    BOOST_TEST(!stream.str().empty());
}

BOOST_AUTO_TEST_CASE(ArgumentsExtra)
{
    std::vector<char const *> opts{"./test", "--foo", "1", "2", "3"};
    int foo{10}, bar{20};

    BOOST_TEST(!config::parse(std::begin(opts), std::end(opts)
        , config::opt(foo, "foo").help("foo value")
        , config::arg(bar, "bar")
    ));
    BOOST_TEST(!stream.str().empty());
}

BOOST_AUTO_TEST_CASE(ArgumentsSink)
{
    std::vector<char const *> opts{"./test", "--foo", "1", "2", "3"};
    int foo{10};
    std::vector<int> bar;

    BOOST_TEST(config::parse(std::begin(opts), std::end(opts)
        , config::opt(foo, "foo").help("foo value")
        , config::arg(bar, "bar")
    ));
    BOOST_TEST(1 == foo);
    BOOST_TEST(stream.str().empty());

    std::vector<int> const expected{2, 3};
    BOOST_TEST(bar == expected);
}

BOOST_AUTO_TEST_SUITE_END()
