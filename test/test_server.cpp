#include <cg/config.hpp>
#include <cg/component.hpp>
#include <tester/metacg.hpp>

BOOST_FIXTURE_TEST_SUITE(Server, tester::Metacg)

struct A : cg::Component
{
    template<typename>
    struct State
    {
        int foo;
        CONFIG_DECLARE_STATE_CONSTRUCTORS(foo) {}
    };

    struct Service
    {
        template<typename Base>
        struct Interface : Base
        {
            int run()
            {
                BOOST_TEST(this->state.foo == 42);

                auto v = this->remote(config::tag::Server{}).template get<int>("/Server/A/foo");
                BOOST_TEST(v == 42);

                auto w = this->remote(config::tag::Server{}).template get<std::string>("/Server::B/bar");
                BOOST_TEST(w == "bar");

                return 0;
            }
        };
    };

    using Services = ct::tuple<Service>;
};

using G = cg::Graph<cg::Connect<A, config::Server>>;

BOOST_AUTO_TEST_CASE(FromFile)
{
    test_graph<G>(std::filesystem::path(__FILE__).replace_extension(".json"));
}

BOOST_AUTO_TEST_SUITE_END()
