#include <cg/config.hpp>
#include <cg/component.hpp>
#include <tester/metacg.hpp>

struct TestIncludeTypes : tester::Metacg
{
    struct tag
    {
        struct B{};
    };
};

BOOST_FIXTURE_TEST_SUITE(Include, TestIncludeTypes)

using tag = TestIncludeTypes::tag;

struct B : cg::Component
{
    template<typename>
    struct State
    {
        int value;
        CONFIG_DECLARE_STATE_CONSTRUCTORS(value) {}
    };

    struct Impl
    {
        template<typename Base>
        struct Interface : Base
        {
            int value() { return this->state.value; }
        };
    };

    using Ports = ct::map<ct::pair<tag::B, Impl>>;
};

struct A : cg::Component
{
    template<typename>
    struct State
    {
        int value;
        CONFIG_DECLARE_STATE_CONSTRUCTORS(value) {}
    };

    struct Service
    {
        template<typename Base>
        struct Interface : Base
        {
            int run()
            {
                BOOST_TEST(this->state.value == 23);
                BOOST_TEST(this->remote(tag::B{}).value() == 42);
                return 0;
            }
        };
    };

    using Services = ct::tuple<Service>;
};

using G = cg::Graph<cg::Connect<A, B>>;

BOOST_AUTO_TEST_CASE(FromFile)
{
    test_graph<G>(std::filesystem::path(__FILE__).replace_extension(".json"));
}

BOOST_AUTO_TEST_SUITE_END()
