#include <cg/info.hpp>
#include <cg/graph.hpp>
#include <cg/component.hpp>
#include <tester/tester.hpp>

BOOST_AUTO_TEST_SUITE(Dot)

struct tag {
    struct Foo {};
    struct Bar {};
    struct Baz {};
};

struct A : cg::Component
{
    struct Impl
    {
        template<typename Base>
        struct Interface : Base {};
    };

    using Ports = ct::map<ct::pair<tag::Foo, Impl>>;
};

struct B : cg::Component
{
    struct Impl
    {
        template<typename Base>
        struct Interface : Base {};
    };

    using Ports = ct::map<
        ct::pair<tag::Bar, Impl>,
        ct::pair<tag::Baz, Impl>
    >;
};

BOOST_AUTO_TEST_CASE(Test)
{
    using G = cg::Graph<
        cg::Connect<A, B, tag::Bar>,
        cg::Connect<A, B, tag::Baz>,
        cg::Connect<B, A>
    >;
    
    auto dot = cg::Info<G>::dot("");
    BOOST_TEST(std::string::npos != dot.find(R"(000 [label="Dot::A"];)"));
    BOOST_TEST(std::string::npos != dot.find(R"(001 [label="Dot::B"];)"));
    BOOST_TEST(std::string::npos != dot.find(R"(000 -> 001 [label="Dot::tag::Bar"];)"));
    BOOST_TEST(std::string::npos != dot.find(R"(000 -> 001 [label="Dot::tag::Baz"];)"));
    BOOST_TEST(std::string::npos != dot.find(R"(001 -> 000 [label="Dot::tag::Foo"];)"));
}

BOOST_AUTO_TEST_SUITE_END()
