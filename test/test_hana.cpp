#include <cg/config.hpp>
#include <cg/component.hpp>
#include <tester/metacg.hpp>

BOOST_FIXTURE_TEST_SUITE(Hana, tester::Metacg)

struct A : cg::Component
{
    template<typename>
    struct Types
    {
        struct Config {
            BOOST_HANA_DEFINE_STRUCT(Config,
                (int, a),
                (std::string, b)
            );
        };
    };

    template<typename Resolver>
    struct State
    {
        typename Types<Resolver>::Config config;
        CONFIG_DECLARE_STATE_CONSTRUCTORS(config) {}
    };

    struct Service
    {
        template<typename Base>
        struct Interface : Base
        {
            int run()
            {
                BOOST_TEST(this->state.config.a == 42);
                BOOST_TEST(this->state.config.b == "foo");
                return 0;
            }
        };
    };

    using Services = ct::tuple<Service>;
};

using G = cg::Graph<A>;

BOOST_AUTO_TEST_CASE(FromFile)
{
    test_graph<G>(std::filesystem::path(__FILE__).replace_extension(".json"));
}

BOOST_AUTO_TEST_SUITE_END()
