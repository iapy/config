#pragma once
#include <cg/component.hpp>
#include <config/tags.hpp>
#include <nlohmann/json.hpp>

namespace config {

struct Server : cg::Component
{
    struct Impl
    {
        template<typename Base>
        struct Interface : Base
        {
            template<typename T>
            T get(std::string const &path)
            {
                nlohmann::json::json_pointer const p{path};
                return this->state.config.at(p).template get<T>();
            };
        };
    };

    template<typename Resolver>
    struct State
    {
        nlohmann::json const config;
        State(nlohmann::json const &config) : config(config) {}
    };

    using Ports = ct::map<ct::pair<tag::Server, Impl>>;
};

}
