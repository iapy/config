#include <algorithm>
#include <iostream>
#include <sstream>
#include <cstring>
#include <iomanip>
#include <vector>

namespace config
{

template<typename T> class arg;
template<typename T> class opt;

namespace detail
{
    template<typename T>
    struct opt_parser
    {
        static constexpr bool multi = false;
        static bool parse(T& target, const char *p)
        {
            std::istringstream(p) >> target;
            return true;
        }
    };

    template<typename T>
    struct opt_parser<std::vector<T>>
    {
        static constexpr bool multi = true;
        static bool parse(std::vector<T>& target, const char *p)
        {
            target.emplace_back();
            std::istringstream(p) >> target.back();
            return false;
        }
    };

    template<typename T> std::ostream &cmd(std::ostream &stream, arg<T> const &);
    template<typename T> std::ostream &cmd(std::ostream &stream, opt<T> const &);
}

template<typename T>
class opt
{
public:
    opt(T &target, const char *long_, char short_ = 0)
    : target {target}
    , long_  {long_}
    , short_ {short_}
    , help_{0}
    {}

    template<typename I>
    bool handle(I &b, I e) const
    {
        if(!accept(*b)) return false;

        if constexpr (std::is_same_v<T, bool>)
        {
            target = true;
            ++b; return true;
        }
        else if(e - b > 1)
        {
            detail::opt_parser<T>::parse(target, *++b);
            ++b; return true;
        }
        else
        {
            return false;
        }
    }

    opt &&help(const char *message) &&
    {
        help_ = message;
        return std::move(*this);
    }

    friend std::ostream &operator << (std::ostream &stream, opt<T> const &v)
    {
        auto const w = stream.width();
        stream << std::setw(0) << "--" << v.long_;
        if(v.short_) stream << " (-" << v.short_ << ")";
        if(v.help_) stream << std::setw(w - v.length() + 1) << ' ' << v.help_;
        return stream << '\n' << std::setw(w);
    }

    std::size_t length() const
    {
        return 2 + std::strlen(long_) + (short_ ? 5 : 0);
    }

    constexpr bool ok() const
    {
        return true;
    }

private:
    bool accept(const char *flag) const
    {
        auto const len = std::strlen(flag);
        return ((len > 2 && !std::strcmp(flag + 2, long_)) || (len == 2 && flag[1] == short_));
    }

private:
    T &target;
    const char *help_;
    const char *long_;
    const char short_;
};

template<typename T>
class arg
{
public:
    arg(T &target, const char *name)
    : target {target}
    , handled {false}
    , name {name}
    {}

    friend std::ostream &operator << (std::ostream &stream, arg<T> const &v)
    {
        return stream;
    }

    template<typename I>
    bool handle(I &b, I e)
    {
        if(handled) return false;
        handled = detail::opt_parser<T>::parse(target, *b++);
        return true;
    }

    std::size_t length() const
    {
        return 0;
    }

    const char *operator + () const
    {
        return name;
    }

    bool ok() const
    {
        return handled || detail::opt_parser<T>::multi;
    }

private:
    T &target;
    const char *name;
    bool handled{false};
};

template<typename T>
arg(T&, const char *) -> arg<T>;

template<typename T>
opt(T&, const char *, char) -> opt<T>;

namespace detail
{
    template<typename T, typename I> bool option(opt<T> &opt, I &b, I e) { return opt.handle(b, e); }
    template<typename T, typename I> bool option(arg<T> &, I &b, I e) { return false; }

    template<typename T, typename I> bool argument(opt<T> &, I &b, I e) { return false; }
    template<typename T, typename I> bool argument(arg<T> &arg, I &b, I e) { return arg.handle(b, e); }

    template<typename T> std::ostream &cmd(std::ostream &stream, arg<T> const &arg)
    {
        stream << " [" << +arg;
        if constexpr (detail::opt_parser<T>::multi) stream << "...";
        stream << "]";
        return stream;
    }

    template<typename T> std::ostream &cmd(std::ostream &stream, opt<T> const &)
    {
        return stream;
    }
}

template<typename I, typename ...T>
bool parse(I b, I e, T&& ...specs)
{
    I prog = b++; 
    auto const help = [&]() {
        std::cerr << *prog;
        int dummy0[] = {(detail::cmd(std::cerr, specs), 0)...};
        (std::cerr << '\n').width(std::max({specs.length()...}));
        int dummy1[] = {(std::cerr << specs, 0)...};
        return false;
    };

    while(b != e)
    {
        if(!(detail::option(specs, b, e) || ...))
        {
            while(b != e && std::strcmp(*b, "-help") && (detail::argument(specs, b, e) || ...));
            if(b != e || !(specs.ok() && ...)) return help(); break;
        }
    }
    return (specs.ok() && ...) || help();
}

template<typename ...T>
void parse(int argc, char **argv, T&&... specs)
{
    if(!parse(argv, argv + argc, std::forward<T>(specs)...)) std::exit(1);
}
} // namespace config
