#pragma once
#include <regex>
#include <fstream>
#include <iostream>
#include <typeinfo>
#include <cg/host.hpp>
#include <config/server.hpp>

#include <boost/core/demangle.hpp>
#include <boost/preprocessor.hpp>
#include <jsonio/deserialize.hpp>

#define CONFIG_STATE_FIELD_CTOR_ARGR(r, data, i, elem) BOOST_PP_COMMA_IF(i) typename cg::json_type<std::remove_cv_t<decltype(elem)>>::type && BOOST_PP_CAT(elem,_)
#define CONFIG_STATE_FIELD_FROM_JSON(r, data, i, elem) BOOST_PP_COMMA_IF(i) \
    [&]{ \
        if(json.contains(BOOST_PP_STRINGIZE(elem))) \
            return std::move(jsonio::from<typename cg::json_type<std::remove_cv_t<decltype(elem)>>::type>(json[BOOST_PP_STRINGIZE(elem)])); \
        return std::move(typename cg::json_type<std::remove_cv_t<decltype(elem)>>::type{}); \
    }()
#define CONFIG_STATE_FIELD_CONSTRUCT(r, data, i, elem) BOOST_PP_COMMA_IF(i) elem{std::move( BOOST_PP_CAT(elem,_) )}

#define CONFIG_DECLARE_STATE_CONSTRUCTORS(...) \
    State(nlohmann::json const &json) \
    : State{BOOST_PP_SEQ_FOR_EACH_I(CONFIG_STATE_FIELD_FROM_JSON, _, BOOST_PP_VARIADIC_TO_SEQ(__VA_ARGS__))} {} \
    State(BOOST_PP_SEQ_FOR_EACH_I(CONFIG_STATE_FIELD_CTOR_ARGR, _, BOOST_PP_VARIADIC_TO_SEQ(__VA_ARGS__))) \
    : BOOST_PP_SEQ_FOR_EACH_I(CONFIG_STATE_FIELD_CONSTRUCT, _, BOOST_PP_VARIADIC_TO_SEQ(__VA_ARGS__))

#define CONFIG_DECLARE_STATE_BASE_CONSTRUCTORS(clazz,...) \
    clazz(nlohmann::json const &json) \
    : clazz{BOOST_PP_SEQ_FOR_EACH_I(CONFIG_STATE_FIELD_FROM_JSON, _, BOOST_PP_VARIADIC_TO_SEQ(__VA_ARGS__))} {} \
    clazz(BOOST_PP_SEQ_FOR_EACH_I(CONFIG_STATE_FIELD_CTOR_ARGR, _, BOOST_PP_VARIADIC_TO_SEQ(__VA_ARGS__))) \
    : BOOST_PP_SEQ_FOR_EACH_I(CONFIG_STATE_FIELD_CONSTRUCT, _, BOOST_PP_VARIADIC_TO_SEQ(__VA_ARGS__))

#define CONFIG_DECLARE_STATE_BASE(Base) \
    State() = delete;                   \
    State(State &&) = default;          \
    State(State const &) = delete;      \
    template<typename ...Args>          \
    State(Args&&... args) : Base{std::forward<Args>(args)...}

namespace cg {

template<typename C>
struct Name
{
    static std::string get()
    {
        return boost::core::demangle(typeid(C).name());
    }
};

namespace detail {

struct FromJsonConfig
{
    template<typename State>
    auto make()
    {
        if constexpr (std::is_constructible_v<decltype(State::state_), nlohmann::json const &>)
        {
            if(config == nullptr)
            {
                if constexpr (std::is_default_constructible_v<decltype(State::state_)>)
                {
                    return State{};
                }
                else
                {
                    throw std::runtime_error{"Cannot find matching constructor for component " + name + " (config = nullptr)"};
                }
            }
            else
            {
                return State{*config};
            }
        }
        else if constexpr (std::is_default_constructible_v<decltype(State::state_)>)
        {
            return State{};
        }
        else
        {
            throw std::runtime_error{"Cannot find matching constructor for class " + name + " (expecting empty constructor)"};
        }
    }

    std::string const name;
    nlohmann::json const *config;
};


template<typename T, typename ...Tail>
struct index_of<T, FromJsonConfig, Tail...>
{
    static constexpr std::size_t value = 1 + index_of<T, Tail...>::value;
};

template<typename States, typename Components>
struct construct_json
{
    static_assert(std::tuple_size_v<States> == std::tuple_size_v<Components>);

    template<
        std::size_t I,
        typename T,
        std::size_t ...P1,
        std::size_t ...P2,
        std::size_t ...P3,
        std::size_t ...P4
    >
    static States from_(nlohmann::json const &config, T&& t, std::index_sequence<P1...>, std::index_sequence<P2...>, std::index_sequence<P3...>, std::index_sequence<P4...>)
    {
        return from<I>(config, std::get<P1>(t)..., std::get<P2>(t)..., std::get<P3>(t)..., std::get<P4>(t)...);
    }

    template<typename ...Tail, std::size_t ...I>
    static States from_(std::tuple<Tail...>&& args, std::index_sequence<I...>)
    {
        return States{std::get<I>(args).template make<std::tuple_element_t<I, States>>()...};
    }

    template<typename C>
    static nlohmann::json const *find_config(nlohmann::json const &config, std::string const &name)
    {
        std::regex rx{"::"};
        std::string const n = name.substr(0, name.find('<'));
        if(auto it = config.find(n); it != config.end())
        {
            return &(*it);
        }
        nlohmann::json const *p = &config;
        for(auto it = std::sregex_token_iterator{n.begin(), n.end(), rx, -1}; it != decltype(it){}; ++it)
        {
            if(p->find(*it) != p->end())
            {
                p = &p->at(*it);
            }
            else
            {
                return nullptr;
            }
        }
        return p;
    }

    template<std::size_t I, typename ...Tail>
    static States from(nlohmann::json const &config, Tail&& ...args)
    {
        if constexpr (I == std::tuple_size_v<States>)
        {
            // tail of recursion
            return from_(std::forward_as_tuple(args...), make_index_range<0, std::tuple_size_v<States>>{});
        }
        else if constexpr (I == sizeof...(Tail))
        {
            using S = std::tuple_element_t<I, States>;
            using C = std::tuple_element_t<I, Components>;

            std::string const name = Name<C>::get();
            if constexpr (std::is_same_v<C, config::Server>)
            {
                return from<I + 1>(config, std::forward<Tail>(args)..., FromJsonConfig{name, &config});
            }
            else
            {
                nlohmann::json const *p = find_config<C>(config, name);
                return from<I + 1>(config, std::forward<Tail>(args)..., FromJsonConfig{name, p});
            }
        }
        else
        {
            using S = std::tuple_element_t<I, States>;
            using C = std::tuple_element_t<I, Components>;
            static constexpr auto J = index_of<C, std::decay_t<Tail>...>::value;

            if constexpr (J == sizeof...(Tail)) 
            {
                std::string const name = Name<C>::get();
                if constexpr (std::is_same_v<C, config::Server>)
                {
                    return from_<I + 1>(
                        config,
                        std::forward_as_tuple(FromJsonConfig{name, &config}, args...),
                        make_index_range<1, I + 1>{},
                        make_index_range<0, 1>{},
                        make_index_range<I + 1, 1 + sizeof...(Tail)>{},
                        std::index_sequence<>{}
                    );
                }
                else
                {
                    nlohmann::json const *p = find_config<C>(config, name); 
                    return from_<I + 1>(
                        config,
                        std::forward_as_tuple(FromJsonConfig{name, p}, args...),
                        make_index_range<1, I + 1>{},
                        make_index_range<0, 1>{},
                        make_index_range<I + 1, 1 + sizeof...(Tail)>{},
                        std::index_sequence<>{}
                    );
                }
            }
            else if constexpr (I == J)
            {
                return from<I + 1>(config, std::forward<Tail>(args)...);
            }
            else
            {
                static_assert(I <= J);
                return from_<I + 1>(
                    config,
                    std::forward_as_tuple(args...),
                    make_index_range<0, I>{},
                    make_index_range<J, J + 1>{},
                    make_index_range<I, J>{},
                    make_index_range<J + 1, sizeof...(Tail)>{}
                );
            }
        }
    }

    template<typename ...A>
    static States from(std::filesystem::path const &p, A&&... args)
    {
        nlohmann::json config;
        { std::ifstream{p} >> config; }
        if(auto it = config.find("$include"); it != config.end())
        {
            for(auto const &path : jsonio::from<std::vector<std::filesystem::path>>(*it))
            {
                nlohmann::json sub;
                if(path.is_absolute())
                {
                    std::ifstream{path} >> sub;
                }
                else
                {
                    std::ifstream{p.parent_path() / path} >> sub;
                }
                config.update(sub, true);
            }
        }
        return from<0>(std::move(config), std::forward<A>(args)...);
    }
};

} // namespace detail

template<typename ...T>
template<typename ...A>
Host<Graph<T...>>::Host(std::filesystem::path &&p, A&&... args)
: states{detail::construct_json<States, Components>::from(p, std::forward<A>(args)...)}
, initializers(this) {}

template<typename ...T>
template<typename ...A>
Host<Graph<T...>>::Host(std::filesystem::path const &p, A&&... args)
: states{detail::construct_json<States, Components>::from(p, std::forward<A>(args)...)}
, initializers(this) {}

template<typename T>
struct json_type
{
    using type = T;
};

} // namespace cg
