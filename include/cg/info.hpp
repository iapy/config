#pragma once
#include <cg/graph.hpp>
#include <boost/core/demangle.hpp>

#include <sstream>
#include <iomanip>
#include <unordered_map>

namespace cg {

CT_TYPE(Info);

template<typename ...Types>
struct Info<Graph<Types...>>
{
    static std::string dot(std::string const &prefix)
    {
        std::unordered_map<std::string, std::size_t> vertices;

        std::ostringstream ss;
        ss << "digraph Components {\n";
        ss << "    node [shape=plaintext];\n";
        vertices_<0, typename Graph<Types...>::type::vertices>(prefix, ss, vertices);
        edges_<0, typename Graph<Types...>::type::edges>(prefix, ss, vertices);
        ss << "}\n";
        return ss.str();
    }
private:
    template<std::size_t I, typename V>
    static void vertices_(std::string const &prefix, std::ostream &stream, std::unordered_map<std::string, std::size_t> &vertices)
    {
        if constexpr (I < V::size)
        {
            using type = ct::get_t<ct::index<I>, V>;
            std::string name = boost::core::demangle(typeid(type).name());
            stream << "    " << prefix << std::setfill('0') << std::setw(3) << vertices.size() << " [label=\"" << name << "\"];\n";
            vertices[std::move(name)] = vertices.size();
            vertices_<I + 1, V>(prefix, stream, vertices);
        }
    }

    template<std::size_t I, typename V>
    static void edges_(std::string const &prefix, std::ostream &stream, std::unordered_map<std::string, std::size_t> const &vertices)
    {
        if constexpr (I < V::keys::size)
        {
            using Vs = ct::get_t<ct::index<I>, typename V::keys>;
            using Es = ct::get_t<ct::index<I>, typename V::vals>;

            using A = ct::get_t<ct::index<0>, Vs>;
            using B = ct::get_t<ct::index<1>, Vs>;

            edges_<0, Es>(
                prefix,
                stream,
                vertices.find(boost::core::demangle(typeid(A).name()))->second,
                vertices.find(boost::core::demangle(typeid(B).name()))->second
            );
            edges_<I + 1, V>(prefix, stream, vertices);
        }
    }

    template<std::size_t I, typename V>
    static void edges_(std::string const &prefix, std::ostream &stream, std::size_t a, std::size_t b)
    {
        if constexpr (I < V::size)
        {
            using N = ct::get_t<ct::index<I>, V>;
            stream << "    " << prefix << std::setfill('0') << std::setw(3) << a
                   << " -> " << prefix << std::setfill('0') << std::setw(3) << b << " [label=\""
                   << boost::core::demangle(typeid(N).name())
                   << "\"];\n";
            edges_<I + 1, V>(prefix, stream, a, b);
        }
    }
};

}
