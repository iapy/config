# Abstract

The `config` library offers the capability to read the state of [metacg](https://bitbucket.org/iapy/metacg) components from a **JSON** file utilising [jsonio](https://bitbucket.org/iapy/jsonio) library for `JSON` serialisation.

## Enabling component configuration

To enable the state of a component to be read from a **JSON** file, it should be defined as follows:

```c++
#include <cg/config.hpp>

namespace demo::components
{
    struct Demo
    {
        template<typename>
        struct State
        {
            int a;
            std::string const b;
            CONFIG_DECLARE_STATE_CONSTRUCTORS(a, b)
            {
                // any additional construction logic
            }
        };
    };
}
```

The `CONFIG_DECLARE_STATE_CONSTRUCTORS` macro is used to define a constructor for a state that is compatible with construction from a **JSON** file and also supports the use of the `cg::Args<>` function.

## Passing config to a component graph

```c++
int main(int argc, char **argv)
{
    using G = cg::Graph<demo::components::Demo>;

    // When using vanilla services
    auto host = cg::Host<G>{
        std::filesystem::path{argv[1]}
    };
    return cg::Service(host);

    // When using msgbus services
    return msgbus::Service<G>(
       std::filesystem::path{argv[1]}
    );
}
```

The configuration file for the graph above can look like:

```json
{
    "demo": {
        "components": {
            "Demo": {
                "a": 42,
                "b": "foo"
            }
        }
    }
}
```

Or even simpler:

```json
{
    "demo::components::Demo": {
        "a": 42,
        "b": "foo"
    }
}
```

You can read the configuration of certain components from a file, while utilising the `cg::Args<>` function to configure other components:

```c++
int main(int argc, char **argv)
{
    using G = cg::Graph<
        my::components::A,
        my::components::B
    >;
    
    // When using vanilla services
    auto host = cg::Host<G>{
       std::filesystem::path{argv[1]},
       cg::Args<my::components::B>(...)
    };
    return cg::Service(host);

    // When using msgbus services
    return msgbus::Service<G>(
       std::filesystem::path{argv[1]},
       cg::Args<my::components::B>(...)
    );
}
```

In the code above, it is assumed that the file provided as `argv[1]` holds the configuration for `my::components::A`.

The configuration file might contain a special directive, `$include`, which specifies a list of either relative (relative to the location of the config file) or absolute paths to configuration files. These files carry the configurations for components that are shared among different applications:

`/opt/etc/config.json`:
```json
{
    "$include": [
        "python.json"
    ],
    "app::Main": {
        ...
    }
}
```

`/opt/etc/python.json`:
```json
{
    "python::Python": {
        "path": "/opt/lib"
    }
}
```

The configurations of components, which are defined in the included files, can be partially overridden:

`/opt/etc/db.json`:
```json
"jsondb::Server": {
    "path": "/opt/data",
    "blueprint": "db"
}
```

`/opt/etc/svc1.json`:
```json
{
    "$include": [
        "python.json"
    ],
    "jsondb::Server": {
        "blueprint": "svc1"
    }
}
```

Configuration above is equivalent to:

```json
{
    "$include": [
        "python.json"
    ],
    "jsondb::Server": {
    .   "path": "/opt/data",
        "blueprint": "svc1"
    }
}
```

## Custom json parser

The component has the potential to implement a custom parser for configurations:

```c++
#include <cg/config.hpp>

namespace demo::components
{
    struct CustomParser
    {
        template<typename>
        struct State
        {
            State(nlohmann::json const &json)
            {
            }
        };
    };
}
```

The `nlohmann::json` object that is passed to the state constructor represents a segment of the configuration under `demo::components::CustomParser`, not the entire configuration.

## config::Server

This library also offers a `config::Server` component. This component comes with an interface labeled as `config::tag::Server`. If incorporated into the graph, it holds the parsed configuration in the form of a `nlohmann::json` object. It also provides a template method named `get` that can be used to access any configuration key:

```c++
template<typename T>
T get(std::string const &path)
```

Considering configuration:

```json
{
    "python::Python": {
        "path": "/opt/lib"
    }
}
```

The client code can access `python::Python.path` in the following manner:
```c++
auto value = this->remote(config::tag::Server{})
	.template get<std::string>("/python::Python/path");
```
